#include <Servo.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>

Servo servoMotorToldo;
Servo servoMotorHumedad;
int SensorPinLuz = A2;
int SensorPinHumedad = A1;
LiquidCrystal_I2C lcd(0x27, 20, 4);

const int trigPin = 11;
const int echoPin = 12;

bool isServoMoved = false;

void setup() {
  lcd.begin(16, 2);
  lcd.init();
  lcd.backlight();
  lcd.print("Inicializando...");
  delay(2000);
  lcd.clear();
  
  Serial.begin(9600);
  
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  servoMotorToldo.attach(8);
  servoMotorHumedad.attach(13);
}

void loop() {
  int valorLuz = analogRead(SensorPinLuz);
  Serial.print("Nivel de luz: ");
  Serial.println(valorLuz);

  if (valorLuz >= 300 && !isServoMoved) {
    servoMotorToldo.write(0);
    delay(500);
    servoMotorToldo.write(180);
    delay(500);
    isServoMoved = true;
  } else if (valorLuz < 299 && isServoMoved) {
    servoMotorToldo.write(180);
    delay(500);
    servoMotorToldo.write(0);
    delay(500);
    isServoMoved = false;
  }

  int humedad = analogRead(SensorPinHumedad);
  Serial.print("Nivel de humedad: ");
  Serial.println(humedad);

  if (humedad >= 799) {
    servoMotorHumedad.write(90);
    delay(500);
  } else if (humedad <= 600) {
    servoMotorHumedad.write(0);
    delay(500);
  }
  
  if (isServoMoved) {
    float distance = getDistance();
    Serial.print("Distancia: ");
    Serial.println(distance);

    if (distance <= 29) {
      servoMotorToldo.write(0);
      delay(500);
    } else if (distance >= 30) {
      servoMotorToldo.write(180);
      delay(500);
    }
  }
  
  lcd.setCursor(0, 0);
  lcd.print("                  ");  
  lcd.setCursor(0, 0);
  if (servoMotorToldo.read() == 180) {
    lcd.print("Toldo Abierto");
  } else if (servoMotorToldo.read() == 0) {
    lcd.print("Toldo Cerrado");
  }
  
  lcd.setCursor(0, 1);
  lcd.print("                    ");  
  lcd.setCursor(0, 1);
  if (servoMotorHumedad.read() == 90) {
    lcd.print("Regando... ");
  } else if (servoMotorHumedad.read() == 0) {
    lcd.print("Humedad: ");
    lcd.println(humedad);
  }
}

float getDistance() {
  float echoTime, cm;

  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  echoTime = pulseIn(echoPin, HIGH);

  cm = echoTime / 29 / 2;
  pulseIn(echoPin, LOW);

  return cm;
}
